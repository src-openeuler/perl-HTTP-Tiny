Name:           perl-HTTP-Tiny
Version:        0.088
Release:        2
Summary:        A small, simple, correct HTTP/1.1 client
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/HTTP-Tiny
Source0:        https://cpan.metacpan.org/authors/id/D/DA/DAGOLDEN/HTTP-Tiny-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  make perl-generators perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict) perl(warnings) perl(bytes) perl(Carp) perl(Errno) perl(Fcntl)
BuildRequires:  perl(IO::Socket) perl(MIME::Base64) perl(Socket) perl(Time::Local)
BuildRequires:  perl(Exporter) perl(File::Basename) perl(File::Spec) perl(File::Temp)
BuildRequires:  perl(IO::Dir) perl(IO::File) perl(IPC::Cmd) perl(lib) perl(open)
BuildRequires:  perl(Test::More) >= 0.96

Requires:       perl(bytes) perl(Carp) perl(Fcntl) perl(MIME::Base64) perl(Time::Local)
Recommends:     perl(IO::Socket::IP) >= 0.32
Recommends:     perl(IO::Socket::SSL) >= 1.56
Recommends:     perl(Mozilla::CA)

%description
This is a very simple HTTP/1.1 client, designed for doing simple requests 
without the overhead of a large framework like LWP::UserAgent.

It is more correct and more complete than HTTP::Lite. It supports proxies and 
redirection. It also correctly resumes after EINTR.

If IO::Socket::IP 0.25 or later is installed, HTTP::Tiny will use it instead 
of IO::Socket::INET for transparent support for both IPv4 and IPv6.

Cookie support requires HTTP::CookieJar or an equivalent class.

%package_help

%prep
%autosetup -n HTTP-Tiny-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR='%{buildroot}'
%{_fixperms} '%{buildroot}'/*

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/*

%files help
%doc Changes CONTRIBUTING.mkdn eg README
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.088-2
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Jul 19 2023 dongyuzhen <dongyuzhen@h-partners.com> - 0.088-1
- upgrade version to 0.088

* Mon Jun 26 2023 yangmingtai <yangmingtai@huawei.com> - 0.080-2
- fix CVE-2023-31486

* Sat Nov 13 2021 yuanxin <yuanxin24@huawei.com> - 0.080-1
- update version to 0.080

* Sat Jan 4 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.076-3
- Delete redundant files

* Tue Sep 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.076-2
- Package init
